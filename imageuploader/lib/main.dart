import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imageuploader/image_picker_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Image Uploader',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.deepPurple,
        ),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool imageUploaded = false;
  XFile? imageFile;

  void startUpload() async {
    setState(() {
      imageUploaded = false;
    });

    final croppedFile = await ImagePickerService().pickCropImage(
      cropAspectRatio: const CropAspectRatio(
        ratioX: 9,
        ratioY: 9,
      ),
      imageSource: ImageSource.camera,
    );

    setState(() {
      imageFile = croppedFile;
    });

    if (imageFile != null) {
      ImagePickerService()
          .uploadFile(
        file: XFile(imageFile!.path),
      )
          .then((uploaded) {
        setState(() {
          imageUploaded = uploaded;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              imageUploaded ? 'image uploaded' : '',
            ),
            imageFile == null
                ? const Text(
                    'You have no image',
                  )
                : Image(
                    image: FileImage(
                      File(imageFile!.path),
                    ),
                  ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: startUpload,
        tooltip: 'Upload Image',
        child: const Icon(
          Icons.camera,
        ),
      ),
    );
  }
}
