import 'package:dio/dio.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerService {
  Future<XFile?> pickCropImage({
    required CropAspectRatio cropAspectRatio,
    required ImageSource imageSource,
  }) async {
    try {
      // Pick image
      XFile? pickImage = await ImagePicker().pickImage(source: imageSource);

      if (pickImage == null) return null;

      // Crop Image
      CroppedFile? cropImage = await ImageCropper().cropImage(
        sourcePath: pickImage.path,
        aspectRatio: cropAspectRatio,
        compressQuality: 90,
        compressFormat: ImageCompressFormat.jpg,
      );

      if (cropImage == null) return null;

      return XFile(cropImage.path);
    } catch (e) {
      return null;
    }
  }

  Future<bool> uploadFile({
    required XFile file,
  }) async {
    try {
      Dio dio = Dio();

      FormData formData = FormData.fromMap({
        'formFiles': await MultipartFile.fromFile(
          file.path,
          filename: 'avatar.jpg',
        ),
      });

      Response response = await dio.post(
        "http://44.225.51.150:5000/upload",
        data: formData,
        options: Options(
          headers: {
            'Authorization':
                'Bearer eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJzdWIiOiI4MyIsInJvbGUiOiIzIiwianRpIjoiMGFlODg5NDktZGFlZC00MjFmLTg1MGYtMjhjNTE5ODgzMWMyIiwiaWF0IjoxNjk2NTA2NzAxLCJuYmYiOjE2OTY1MDY3MDEsImV4cCI6MTY5NjU3ODcwMSwiaXNzIjoiTWFnaWxhdGVjaCIsImF1ZCI6IlV0YWxpaSJ9.CNDKGcXZajuqwFuNKeOnL7HHoH9dQ78gKGlzac-5eP0',
          },
        ),
      );

      if (response.statusCode == 200) {
        // File uploaded successfully.
        print("Upload successful ${response.data}");
        return true;
      } else {
        // Handle HTTP error.
        print("HTTP Error: ${response.statusCode}");
        return false;
      }
    } catch (e) {
      // Handle other errors.
      print("Error: $e");
      return false;
    }
  }
}
